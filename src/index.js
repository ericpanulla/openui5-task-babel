const babelCompiler = require('../processors/babelCompiler');

const path = require('path');
const jsYaml = require('js-yaml');
const fs = require('fs');

/**
 * @param {Object} parameters Parameters
 * @param {module:@ui5/fs.DuplexCollection} parameters.workspace DuplexCollection to read and write files
 * @param {module:@ui5/fs.AbstractReader} parameters.dependencies Reader or Collection to read dependency files
 * @param {Object} parameters.options Options
 * @param {string} parameters.options.projectName Project name
 * @param {string} [parameters.options.configuration] Task configuration if given in ui5.yaml
 * @returns {Promise<undefined>} Promise resolving with <code>undefined</code> once data has been written
 */
module.exports = async function({ workspace, dependencies, options }) {
  const configPath = path.resolve('ui5.yaml');
  if (!configPath) {
    throw new Error('No ui5.yaml file. Make sure this is a UI5 project');
  }

  const configFile = fs.readFileSync(configPath, 'utf8');
  const mainConfig = jsYaml.safeLoadAll(configFile, { json: true })[0];
  const namespace = mainConfig.metadata.name;

  let srcRoot = './webapp';
  if (mainConfig.type === 'library') {
    srcRoot = './src';
  }

  const resources = await workspace.byGlob(['**/*.js']);
  const transformedResources = await babelCompiler({
    resources,
    namespace,
    srcRoot,
  });

  await Promise.all(
    transformedResources.map((resource) => {
      return workspace.write(resource);
    })
  );
};
