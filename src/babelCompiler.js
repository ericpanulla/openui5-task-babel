const babel = require('@babel/core');

/**
 * Compiles ES6 code using @babel/core with .babelrc configuration
 */
class BabelCompiler {
  constructor({ namespace, srcRoot }) {
    this.namespace = namespace;
    this.srcRoot = srcRoot;
  }

  /**
   * Compiled the provided resources
   *
   * @public
   * @param {module:@ui5/fs.Resource[]} resources Raw es6 JavaScript files
   * @returns {Promise<module:@ui5/fs.Resource[]>} Resolving with array of transformed files
   */
  compile(resources) {
    const transform = (resource) => {
      const physPath = this.getPhysicalPath(
        resource.getPath(),
        this.namespace,
        this.srcRoot
      );
      return resource.getString().then((str) => {
        return babel.transformAsync(str, { filename: physPath }).then((result) => {
          resource.setString(result.code);
          return resource;
        });
      });
    };

    return Promise.all(
      resources.map((res) => {
        return transform(res);
      })
    );
  }

  /**
   * Retrieves the physical file path of the app relative to the app root
   *
   * @param {String} virtualPath The virtual resource path provided by UI5 Tooling
   * @param {String} namespace The app namespace i.e. sap.ui.demo
   * @param {String} srcRoot The root of the app source i.e. /webapp
   */
  getPhysicalPath(virtualPath, namespace, srcRoot) {
    return virtualPath.replace(`/resources/${namespace}`, srcRoot);
  }
}

/**
 * Compiles ES6 code using @babel/core with .babelrc configuration
 *
 * @public
 * @alias module:processors.babelCompiler
 * @param {Object} parameters Parameters
 * @param {module:@ui5/fs.Resource[]} parameters.resources List of <code>.js</code> resources to be processed
 * @param {String} parameters.namespace The project namespace
 * @param {String} parameters.srcRoot The project src root i.e. ./webapp
 * @returns {Promise<module:@ui5/fs.Resource[]>} Promise resolving with theme resources
 */
module.exports = ({ resources, namespace, srcRoot }) => {
  const babelCompiler = new BabelCompiler({ namespace, srcRoot });
  return babelCompiler.compile(resources);
};

module.exports.BabelCompiler = BabelCompiler;
