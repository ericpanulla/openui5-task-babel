# openui5-task-babel
This package contains a custom @ui5/cli task for compiling ES6 code with @babel/core. It uses the default .babelrc configuration.

# Include this in your ui5.yaml as a ui5 dependency
https://sap.github.io/ui5-tooling/pages/extensibility/CustomTasks/

# Project configuration
```
specVersion: "1.0"
kind: project
type: library
metadata:
  name: my.library
builder:
  customTasks:
    - name: generateMarkdownFiles
      afterTask: uglify
      configuration:
        color: blue
---
```
# Task extension as part of your projects
```
specVersion: "1.0"
kind: extension
type: task
metadata:
  name: generateMarkdownFiles
task:
  path: lib/tasks/generateMarkdownFiles.js
```